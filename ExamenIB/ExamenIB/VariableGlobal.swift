//
//  VariableGlobal.swift
//  ExamenIB
//
//  Created by Katherine Hurtado on 13/12/17.
//  Copyright © 2017 Katherine Hurtado. All rights reserved.
//

import Foundation

var movie = Movie()

class Movie {
    var name:String?
    var summary:String?
    var imageUrl:String?
}
