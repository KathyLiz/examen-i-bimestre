//
//  BackendManager.swift
//  ExamenIB
//
//  Created by Katherine Hurtado on 13/12/17.
//  Copyright © 2017 Katherine Hurtado. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import AlamofireImage

class BackendManager{

    func getMovies(movieTitle:String){
        Alamofire.request("http://api.tvmaze.com/singlesearch/shows?q=\(movieTitle)").responseObject { (response: DataResponse<MarvelHero>) in
            
            let marvelhero = response.result.value
            
            print(marvelhero?.name ?? "NO existe");
            print(marvelhero?.imageUrl ?? "No existe");
            print(marvelhero?.summary ?? "No existe");
            
            DispatchQueue.main.async {
                movie.name = marvelhero?.name
                movie.imageUrl = marvelhero?.imageUrl
                movie.summary = marvelhero?.summary
             }
        }
       
    }
    
    
    func getImage(_ url:String, completionHandler: @escaping(UIImage) ->()){
        
        print(url)
        Alamofire.request(url).responseImage { response in
            
            if let image = response.result.value {
                completionHandler(image)
            }
        }
        
    }
}
