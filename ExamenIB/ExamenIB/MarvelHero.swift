//
//  MarvelHero.swift
//  ExamenIB
//
//  Created by Katherine Hurtado on 11/12/17.
//  Copyright © 2017 Katherine Hurtado. All rights reserved.
//

import Foundation
import ObjectMapper
class MarvelHero : Mappable{
    var name:String?
    var summary:String?
    var imageUrl:String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        summary <- map ["summary"]
        imageUrl <- map ["image.medium"]
    }
}
