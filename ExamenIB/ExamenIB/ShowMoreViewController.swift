//
//  ShowMoreViewController.swift
//  ExamenIB
//
//  Created by Katherine Hurtado on 13/12/17.
//  Copyright © 2017 Katherine Hurtado. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ShowMoreViewController: UIViewController {
    
    var nombre = ""
    var urlImage = ""
    var summary = ""
    @IBOutlet weak var labelNombre: UILabel!
    @IBOutlet weak var labelDesc: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        labelNombre.text=nombre;
        labelDesc.text=summary;
        
        // Do any additional setup after loading the view.
    }

}
