//
//  ViewController.swift
//  ExamenIB
//
//  Created by Katherine Hurtado on 11/12/17.
//  Copyright © 2017 Katherine Hurtado. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import PopupDialog

class ViewController: UIViewController {
     @IBOutlet weak var searchTextField: UITextField!
    let apikey="26f4d7cffbb3281939398bf593fe34f5";
    
    @IBAction func SearchButtonPressed(_ sender: Any) {
        Alamofire.request("http://api.tvmaze.com/singlesearch/shows?q=\(searchTextField.text!)").responseObject { (response: DataResponse<MarvelHero>) in
            
            let marvelhero = response.result.value
            
            print(marvelhero?.name ?? "NO existe");
            print(marvelhero?.imageUrl ?? "No existe");
             print(marvelhero?.summary ?? "No existe");
            /*DispatchQueue.main.async {
                self.nameLabel.text = pokemon?.name ?? ""
                self.heightLabel.text = "\(pokemon?.height ?? 0)"
                self.weightLabel.text = "\(pokemon?.weight ?? 0)"
            }*/
            self.showImageDialog(marvelhero: marvelhero!)
        }
        
        
        
        
    }
    
    func showImageDialog(animated: Bool = true, marvelhero: MarvelHero) {
        
        // Prepare the popup assets
        let title = marvelhero.name
        
        let message = marvelhero.summary
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)
        
        // Boton cancelar
        let cancelar = CancelButton(title: "Cerrar") {
            print("Retorna a la lista de peliculas")
        }
        
        // Add buttons to dialog
        popup.addButtons([cancelar])
        
        // Present dialog
        self.present(popup, animated: animated, completion: nil)
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier=="SearchMovie"){
            let destino = segue.destination as! ShowMoreViewController
            
            destino.nombre = "Texto desde View Controller"
            destino.summary = "hey";
        }
    }



}

